package com.woniu.mp.mapstruct.third;

import lombok.Data;

import java.util.Date;

@Data
public class Car {
    /**
     * 唯一id
     */
    private String id;

    private Date creationDate;

    private String otherName;
}

package com.woniu.mp.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.woniu.mp.entity.CategoryEntity;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}

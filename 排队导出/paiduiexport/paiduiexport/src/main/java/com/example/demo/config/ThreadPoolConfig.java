package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Configuration
public class ThreadPoolConfig {

    private static final int corePoolSize = 10; // 核心线程数
    private static final int maximumPoolSize = 20; // 最大线程数
    private static final long keepAliveTime = 60L; // 空闲线程存活时间
    private static final TimeUnit TIME_UNIT = TimeUnit.SECONDS;

    @Bean
    public ThreadPoolExecutor getThreadPoolExecutor() {
        return new ThreadPoolExecutor(
                corePoolSize,
                maximumPoolSize,
                keepAliveTime,
                TIME_UNIT,
                new ArrayBlockingQueue<>(2000),
                new ThreadPoolExecutor.AbortPolicy()
        );
    }

}

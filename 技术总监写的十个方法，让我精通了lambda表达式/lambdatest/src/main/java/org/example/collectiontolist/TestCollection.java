package org.example.collectiontolist;


import com.google.common.collect.Lists;
import org.example.entity.OrderItem;
import org.junit.Test;

import java.util.List;
import java.util.Set;

public class TestCollection {

    //将集合 Collection 转化为 List
    @Test
    public void testToList() {
        List<OrderItem> orderItems = Lists.newArrayList(
                new OrderItem(1, 5d, "手表"),
                new OrderItem(2, 6d, "机器人"),
                new OrderItem(3, 8d, "手机")
        );
        List<OrderItem> list = CollectionToListOrSet.toList(orderItems);
    }

    //将集合 Collection 转化为 Set
    @Test
    public void testToSet() {
        List<OrderItem> orderItems = Lists.newArrayList(
                new OrderItem(1, 5d, "手表"),
                new OrderItem(2, 6d, "机器人"),
                new OrderItem(3, 8d, "手机")
        );
        Set<OrderItem> set = CollectionToListOrSet.toSet(orderItems);
    }

}

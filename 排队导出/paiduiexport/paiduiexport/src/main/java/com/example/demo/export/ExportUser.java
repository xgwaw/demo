package com.example.demo.export;

import lombok.Data;

@Data
public class ExportUser {
    private String userName;
}

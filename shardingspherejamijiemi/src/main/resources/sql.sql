SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for tb_order
-- ----------------------------
DROP TABLE IF EXISTS `tb_order`;
CREATE TABLE `tb_order` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `order_id` varchar(50) DEFAULT NULL COMMENT '订单号',
  `name` varchar(100) DEFAULT NULL COMMENT '用户姓名',
  `id_card` varchar(100) DEFAULT NULL COMMENT '身份证号',
  `order_type` char(5) DEFAULT NULL COMMENT '订单类型，1-挂号，2-门诊，3-住院。',
  `order_status` char(5) DEFAULT NULL COMMENT '订单状态，0-待支付，1-支付成功，2-支付失败。',
  `amount` varchar(20) DEFAULT NULL COMMENT '订单金额',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of tb_order
-- ----------------------------
INSERT INTO `tb_order` VALUES ('5', 'wx2202210122003344763267816', '3qxlE2jBqAM+wkPYM2Drgg==', 'MPLHKUIwav77Ad+9xe+O9olTwNArU7thbEmljIkKtxY=', '1', '0', '15', '2022-02-21 09:23:45', '2022-02-21 09:23:45');

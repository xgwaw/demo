package com.woniu.mp.mapstruct.four;

import lombok.Data;

@Data
public class StudentDto {
    private String name;
}
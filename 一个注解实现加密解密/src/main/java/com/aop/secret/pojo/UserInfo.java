package com.aop.secret.pojo;

import lombok.Data;


@Data
public class UserInfo {
	private Long id;
	private String name;
	private Integer age;
	private String idCard;
	private String phone;
	private String address;
}

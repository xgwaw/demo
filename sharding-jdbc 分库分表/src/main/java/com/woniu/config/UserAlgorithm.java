package com.woniu.config;


import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Collection;

@Component
public class UserAlgorithm implements PreciseShardingAlgorithm<Long> {
    @Override
    public String doSharding(Collection<String> tables, PreciseShardingValue<Long> preciseShardingValue) {
        if (!StringUtils.isEmpty(preciseShardingValue)) {
            Long value = preciseShardingValue.getValue();
            Long index = getIndex(value);
            for (String table : tables) {
                if (table.endsWith(index+"")) {
                    return table;
                }
            }
        }
        return null;
    }

    private Long getIndex(Long value) {
        return value % 2 + 1;
    }


}

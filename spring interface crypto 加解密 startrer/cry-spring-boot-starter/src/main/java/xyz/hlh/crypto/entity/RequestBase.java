package xyz.hlh.crypto.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author wn
 * @description:
 */
@Getter
@Setter
@EqualsAndHashCode
public class RequestBase {

    private Long currentTimeMillis;

}

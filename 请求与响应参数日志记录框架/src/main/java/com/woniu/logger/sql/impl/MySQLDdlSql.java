package com.woniu.logger.sql.impl;


import cn.hutool.core.util.StrUtil;
import com.woniu.logger.entity.LoggerInfoEntity;
import com.woniu.logger.sql.IDdlSql;

public class MySQLDdlSql implements IDdlSql {

    @Override
    public String queryTable(String date) {
        return "select count(1) from information_schema.tables where table_name ='" + LoggerInfoEntity.TABLE_NAME + (StrUtil.isNotBlank(date) ? "_" + date : "") + "';";
    }

    @Override
    public String createTable() {
        return "create table " + LoggerInfoEntity.TABLE_NAME + "(" +
                "  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',\n" +
                "  `url` varchar(500) COMMENT '访问的url',\n" +
                "  `class_name` varchar(500) COMMENT '类名',\n" +
                "  `method_name` varchar(100) COMMENT '方法名',\n" +
                "  `req_ip_adr` varchar(20) COMMENT '请求的ip地址',\n" +
                "  `rsp_ip_adr` varchar(20) COMMENT '响应的ip地址',\n" +
                "  `success_ind` tinyint COMMENT '成功标志',\n" +
                "  `req_header` text COMMENT '请求报文头',\n" +
                "  `req_body` text COMMENT '请求报文体',\n" +
                "  `rsp_body` text COMMENT '响应报文体',\n" +
                "  `error_msg` text COMMENT '错误信息',\n" +
                "  `total_time` Long COMMENT '总耗时',\n" +
                "  `create_time` datetime DEFAULT NULL COMMENT '创建时间',\n" +
                "  PRIMARY KEY (`id`) USING BTREE,\n" +
                "  KEY `idx_name` (`url`) USING BTREE\n" +
                ") ENGINE=InnoDB COMMENT='日志信息';";
    }

    @Override
    public String backTable(String date) {
        return "rename table " + LoggerInfoEntity.TABLE_NAME + " to " + LoggerInfoEntity.TABLE_NAME + "_" + date + ";";
    }

    @Override
    public String dropTable() {
        return "drop table " + LoggerInfoEntity.TABLE_NAME + ";";
    }

}
